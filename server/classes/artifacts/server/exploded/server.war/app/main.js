/**
 * Created by dewmal on 8/11/14.
 */
var app = angular.module('zoomapp', ['ui.bootstrap']);

function postController($scope, $window, $modal) {


    $scope.categoryList = [];
    $scope.category = [];
    $scope.sub_category = [];


    $window.init = function () {
        console.log("working")

        $scope.categoryList = [];
        $scope.category = [];
        $scope.sub_category = [];
        $scope.$apply($scope.load_grackel_lib);

    };

    $scope.load_grackel_lib = function () {

       // gapi.client.setApiKey("AIzaSyBwcRb-zw6de9UIISCzS8upCaNzlNY6fSE");

        var apiName = 'posting'
        var apiVersion = 'v1'
        // set the apiRoot to work on a deployed app and locally
        var apiRoot = '//' + window.location.host + '/_ah/api';
        var callbackPost = function () {
            console.log(gapi.client);

//            gapi.client.posting.get({id: '6614661952700416'}).execute(
//                function (response) {
//                    console.log(response);
//                });
        };
        gapi.client.load(apiName, apiVersion, callbackPost, apiRoot);


        var callbackCategory = function () {
            console.log(gapi.client);

            gapi.client.category.list().execute(
                function (response) {
                    console.log(response);
                    $scope.categoryList = response.items;
                    angular.forEach($scope.categoryList, function (value, key) {
                        if (!value.parentCategory) {
                            $scope.category.push(value);
                        }
                    });
                    if ($scope.category[0]) {
                        $scope.post.category = $scope.category[0];
                        $scope.loadSubCategories($scope.post.category);
                        $scope.$apply();
                    }
                });
        };
        gapi.client.load("category", apiVersion, callbackCategory, apiRoot);
    };


    $scope.loadSubCategories = function (id) {
        $scope.sub_category = [];

        console.log(id);
        angular.forEach($scope.categoryList, function (value, key) {
            if (value.parentCategory == id.id) {
                $scope.sub_category.push(value);
            }

        });
        if ($scope.sub_category[0]) {
            $scope.post.subCategory = $scope.sub_category[0];
        }
        // $scope.$apply();
    }


    $scope.post = {

        resourceses: [

            {

                name: 'Banner Image',
                resURL: null,
                isres: true,
                cannotRemove: true

            }

        ]
    };


    $scope.btSave = function () {

        var post = {};
        angular.copy($scope.post, post);

        if (post.category) {
            post.category = $scope.post.category;
        }

        if (post.subCategory) {
            post.subCategory = $scope.post.subCategory;
        }

        console.log(post);

        gapi.client.posting.save(post).execute(
            function (response) {
                console.log(response);
            });
    }


    $scope.btAddFile = function () {
        $scope.post.resourceses.push(
            {

                name: 'Banner Image',
                resURL: null,
                isres: true,
                cannotRemove: false

            }
        );
    }

    $scope.btRemoveFile = function (index) {
        console.log(index, $scope.post.files)
        $scope.post.resourceses.splice(index, 1);
    }


    $scope.open = function (size, index) {

        var modalInstance = $modal.open({
            templateUrl: 'addCategory.html',
            controller: ModalInstanceCtrl,
            size: size,
            resolve: {
                category: function () {
                    var category = [];
                    angular.copy($scope.category, category);
                    return category;
                },
                parentCategory: function () {
                    var id = -1000;
                    console.log(index);
                    if (index) {

                        angular.forEach($scope.category, function (value, key) {
                            console.log(key, value, $scope.post.category);
                            if (angular.equals($scope.post.category.id, value.id)) {
                                id = key;
                            }
                        });
                    }
                    return id;
                }
            }
        });
    }


}

var ModalInstanceCtrl = function ($scope, $window, $modalInstance, category, parentCategory) {

    $scope.categorylIST = category;

    parentCategory++;
    $scope.categorylIST.unshift({
            id: 0,
            category: "New Category"
        }
    );


    if (parentCategory == -999) {
        parentCategory = 0;
    }
    console.log(parentCategory);
    $scope.categoryObj = {
        parentCategory: $scope.categorylIST[parentCategory]
    }

    $scope.saveCategory = function (category) {


        if (category.parentCategory.id == 0) {
            delete  category.parentCategory;
        } else {
            category.parentCategory = category.parentCategory.id;
        }

        console.log(category);

        gapi.client.category.save(category).execute(function (response) {
            console.log(response);
            $window.init();
            $modalInstance.dismiss('cancel');
        });


    }

    $scope.cancelSave = function () {
        $modalInstance.dismiss('cancel');
    }


};