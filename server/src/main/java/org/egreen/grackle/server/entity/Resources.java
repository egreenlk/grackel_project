package org.egreen.grackle.server.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by dewmal on 8/16/14.
 */
@Entity
public class Resources {

    @Id
    private Long id;

    @Index
    private String name;

    @Index
    private String resURL;

    private boolean isres;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResURL() {
        return resURL;
    }

    public void setResURL(String resURL) {
        this.resURL = resURL;
    }

    public boolean isIsres() {
        return isres;
    }

    public void setIsres(boolean isres) {
        this.isres = isres;
    }


    @Override
    public String toString() {
        return "Resources{" +
                "name='" + name + '\'' +
                ", resURL='" + resURL + '\'' +
                ", isres=" + isres +
                '}';
    }
}
