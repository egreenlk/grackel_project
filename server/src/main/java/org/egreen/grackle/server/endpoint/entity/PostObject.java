package org.egreen.grackle.server.endpoint.entity;

import com.googlecode.objectify.Key;
import org.egreen.grackle.server.OfyService;
import org.egreen.grackle.server.entity.Category;
import org.egreen.grackle.server.entity.Post;
import org.egreen.grackle.server.entity.Resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dewmal on 8/16/14.
 */

public class PostObject {
    private Long id;
    private String title;
    private String content;

    private Date postDate;

    private boolean isPublished;

    private CategoryObject category;

    private CategoryObject subCategory;


    private List<Resources> resourceses;


    public PostObject() {
    }

    public PostObject(Post post) {

        title = post.getTitle();
        content = post.getContent();
        postDate = post.getPostDate();
        isPublished = post.isPublished();
        if (post.getCategory() != null) {

            Category categoryObj = OfyService.ofy().load().key(post.getCategory()).now();
            if (categoryObj != null) {
                category = new CategoryObject(categoryObj);
            }
        }

        if (post.getSubCategory() != null) {
            Category categoryObj = OfyService.ofy().load().key(post.getSubCategory()).now();
            if (categoryObj != null) {
                subCategory = new CategoryObject(categoryObj);
            }
        }
        resourceses = new ArrayList<Resources>();

        List<Key<Resources>> resourceses1 = post.getResourceses();
        for (Key<Resources> key : resourceses1) {
            Resources resources = OfyService.ofy().load().key(key).now();
            resourceses.add(resources);
        }


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public CategoryObject getCategory() {
        return category;
    }

    public void setCategory(CategoryObject category) {
        this.category = category;
    }

    public CategoryObject getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(CategoryObject subCategory) {
        this.subCategory = subCategory;
    }

    public List<Resources> getResourceses() {
        return resourceses;
    }

    public void setResourceses(List<Resources> resourceses) {
        this.resourceses = resourceses;
    }
}
