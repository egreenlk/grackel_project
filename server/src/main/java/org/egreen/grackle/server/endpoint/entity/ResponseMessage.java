package org.egreen.grackle.server.endpoint.entity;

/**
 * Created by dewmal on 8/16/14.
 */
public class ResponseMessage {

    private int type;
    private String message;

    public ResponseMessage(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
