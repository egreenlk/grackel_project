package org.egreen.grackle.server.entity;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import org.egreen.grackle.server.endpoint.entity.CategoryObject;

/**
 * Created by dewmal on 8/16/14.
 */
@Entity
public class Category {

    @Id
    private Long id;
    private Key<Category> parentCategory;
    @Index
    private String category;

    public Category(CategoryObject categoryObject) {

        if (categoryObject.getParentCategory() != null) {
            Key<Category> categoryKey = Key.create(Category.class, categoryObject.getParentCategory());
            parentCategory = categoryKey;
        }
        category = categoryObject.getCategory();

    }

    public Category() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Key<Category> getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Key<Category> parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
