package org.egreen.grackle.server.entity;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import org.egreen.grackle.server.endpoint.entity.PostObject;

import java.util.Date;
import java.util.List;

/**
 * Created by dewmal on 8/12/14.
 */
@Entity
public class Post {

    @Id
    private Long id;

    private String title;

    private String content;

    @Index
    private Date postDate;

    private boolean isPublished;

    @Index
    private Key<Category> category;

    @Index
    private Key<Category> subCategory;


    private List<Key<Resources>> resourceses;

    public Post() {
    }

    public Post(PostObject post) {
        title = post.getTitle();
        content = post.getContent();
        postDate = new Date();
        isPublished = post.isPublished();


        if (post.getCategory() != null) {
            Key<Category> categoryKey = Key.create(Category.class, post.getCategory().getId());
            category = categoryKey;
        }


        if (post.getSubCategory() != null) {
            Key<Category> categoryKey = Key.create(Category.class, post.getSubCategory().getId());
            subCategory = categoryKey;
        }
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public boolean isPublished() {
        return isPublished;
    }

    public void setPublished(boolean isPublished) {
        this.isPublished = isPublished;
    }

    public Key<Category> getCategory() {
        return category;
    }

    public void setCategory(Key<Category> category) {
        this.category = category;
    }

    public Key<Category> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Key<Category> subCategory) {
        this.subCategory = subCategory;
    }


    public List<Key<Resources>> getResourceses() {
        return resourceses;
    }

    public void setResourceses(List<Key<Resources>> resourceses) {
        this.resourceses = resourceses;
    }


}
