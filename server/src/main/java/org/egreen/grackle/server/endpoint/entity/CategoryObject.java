package org.egreen.grackle.server.endpoint.entity;

import org.egreen.grackle.server.entity.Category;

/**
 * Created by dewmal on 8/16/14.
 */
public class CategoryObject {

    private Long id;
    private Long parentCategory;
    private String category;

    public CategoryObject() {
    }

    public CategoryObject(Category category) {

        id = category.getId();
        if (category.getParentCategory() != null) {
            parentCategory = category.getParentCategory().getId();
        }
        this.category = category.getCategory();


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Long parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
