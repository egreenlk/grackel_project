package org.egreen.grackle.server.endpoint;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import org.egreen.grackle.server.RegistrationRecord;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import static org.egreen.grackle.server.OfyService.ofy;

/**
 * Created by dewmal on 8/18/14.
 */
public class MessageSendHandler {
    private static final Logger log = Logger.getLogger(MessageSendHandler.class.getName());

    private static final String API_KEY = System.getProperty("gcm.api.key");

    public void sendMessage() throws IOException {

        Sender sender = new Sender(API_KEY);

        Message msg = new Message.Builder().addData("message", "I have a new message for you").build();


        List<RegistrationRecord> records = ofy().load().type(RegistrationRecord.class).list();
        for (RegistrationRecord record : records) {
            Result result = null;

            result = sender.send(msg, record.getRegId(), 5);

            if (result.getMessageId() != null) {
                log.info("Message sent to " + record.getRegId());
                String canonicalRegId = result.getCanonicalRegistrationId();
                if (canonicalRegId != null) {
                    // if the regId changed, we have to update the datastore
                    log.info("Registration Id changed for " + record.getRegId() + " updating to " + canonicalRegId);
                    record.setRegId(canonicalRegId);
                    ofy().save().entity(record).now();
                }
            } else {
                String error = result.getErrorCodeName();
                if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
                    log.warning("Registration Id " + record.getRegId() + " no longer registered with GCM, removing from datastore");
                    // if the device is no longer registered with Gcm, remove it from the datastore
                    ofy().delete().entity(record).now();
                } else {
                    log.warning("Error when sending message : " + error);
                }
            }
        }
    }

}
