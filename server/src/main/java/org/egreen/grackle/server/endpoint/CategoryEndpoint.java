package org.egreen.grackle.server.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.LoadType;
import org.egreen.grackle.server.OfyService;
import org.egreen.grackle.server.endpoint.entity.CategoryObject;
import org.egreen.grackle.server.endpoint.entity.ResponseMessage;
import org.egreen.grackle.server.entity.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dewmal on 8/16/14.
 */
@Api(name = "category",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "server.grackle.egreen.org",
                ownerName = "server.grackle.egreen.org",
                packagePath = "")
)
public class CategoryEndpoint {

    @ApiMethod(name = "save")
    public ResponseMessage save(CategoryObject category) {
        Category categoryEntity = new Category(category);
        Key<Category> now = OfyService.ofy().save().entity(categoryEntity).now();

        return new ResponseMessage(1, "Done SAve" + now.getId());
    }


    @ApiMethod(name = "get")
    public CategoryObject get(@Named("id") Long id) {
        Key<Category> categoryKey = Key.create(Category.class, id);
        Category category = OfyService.ofy().load().type(Category.class).filterKey(categoryKey).first().now();
        if (category != null) {
            return new CategoryObject(category);
        } else {
            return new CategoryObject();
        }
    }

    @ApiMethod(name = "list")
    public List<CategoryObject> getAll() {
        LoadType<Category> categoryList = OfyService.ofy().load().type(Category.class);
        List<CategoryObject> categoryObjectList = new ArrayList<CategoryObject>();
        for (Category cate : categoryList) {
            categoryObjectList.add(new CategoryObject(cate));
        }
        return categoryObjectList;
    }

}
