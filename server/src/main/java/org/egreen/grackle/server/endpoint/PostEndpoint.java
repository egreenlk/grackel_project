package org.egreen.grackle.server.endpoint;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.SimpleQuery;
import org.egreen.grackle.server.OfyService;
import org.egreen.grackle.server.endpoint.entity.PostObject;
import org.egreen.grackle.server.entity.Category;
import org.egreen.grackle.server.entity.Post;
import org.egreen.grackle.server.entity.Resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by dewmal on 8/12/14.
 */
@Api(name = "posting",
        version = "v1",
        namespace = @ApiNamespace(
                ownerDomain = "server.grackle.egreen.org",
                ownerName = "server.grackle.egreen.org",
                packagePath = "")
)
public class PostEndpoint {


    /**
     * Api Keys can be obtained from the google cloud console
     */

    @ApiMethod(name = "get")
    public PostObject getPosts(@Named("id") String id) {
        Key<Post> postKey = Key.create(Post.class, Long.parseLong(id));
        SimpleQuery<Post> now = OfyService.ofy().load().type(Post.class).filterKey(postKey);
        Post post1 = now.first().now();
        PostObject post = new PostObject();
        if (post1 != null) {
            post = new PostObject(
                    post1
            );
        }
        return post;
    }


    @ApiMethod(name = "getall")
    public List<PostObject> getAllPosts(@Named("limit") int limit, @Named("offset") int offset) {
        List<Post> postList = OfyService.ofy().load().type(Post.class).limit(limit).offset(offset).order("-postDate").list();

        List<PostObject> postObjects = new ArrayList<PostObject>();

        for (Post post : postList) {
            postObjects.add(new PostObject(post));
        }

        return postObjects;


    }

    @ApiMethod(name = "getallbycategory")
    public List<PostObject> getAllPostsByCategory(@Named("limit") int limit, @Named("offset") int offset, @Named("cat_id") long cat_id) {

        Key<Category> categoryKey = Key.create(Category.class, cat_id);

      //  System.out.println(categoryKey);

        List<Post> postList = OfyService.ofy().load().type(Post.class).filter("category",categoryKey).limit(limit).offset(offset).order("-postDate").list();

        List<PostObject> postObjects = new ArrayList<PostObject>();

        for (Post post : postList) {
            postObjects.add(new PostObject(post));
        }

        return postObjects;


    }


    @ApiMethod(name = "save")
    public void savePost(PostObject post) {

        List<Resources> resourceses = post.getResourceses();

        List<Key<Resources>> resourcesKeys = new ArrayList<Key<Resources>>();

        for (Resources resources : resourceses) {
            System.out.println(resources);
            Key<Resources> now = OfyService.ofy().save().entity(resources).now();
            System.out.println(resources);
            resourcesKeys.add(now);
        }


        Post postEntity = new Post(post);
        postEntity.setResourceses(resourcesKeys);

        OfyService.ofy().save().entity(postEntity).now();
        System.out.println(postEntity);


        MessageSendHandler sendHandler = new MessageSendHandler();
        try {
            sendHandler.sendMessage();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
