package org.egreen.grackle.utils;

public class SinglishConverter {
	 public String sinToUnI(String input)
	  {
	    try
	    {
	      int vowels = 18;
	      

	      String[] vowelsUni = new String[vowels];
	      String[] vowelSinglish = new String[vowels];
	      String[] vowelsSigns = new String[vowels];
	      

	      vowelsUni[0] = "ආ";vowelSinglish[0] = "aa";vowelsSigns[0] = "ා";
	      vowelsUni[1] = "ඇ";vowelSinglish[1] = "ae";vowelsSigns[1] = "ැ";
	      vowelsUni[2] = "ඖ";vowelSinglish[2] = "au";vowelsSigns[2] = "ෞ";
	      vowelsUni[3] = "අ";vowelSinglish[3] = "a";vowelsSigns[3] = "";
	      vowelsUni[4] = "ඈ";vowelSinglish[4] = "A";vowelsSigns[4] = "ෑ";
	      vowelsUni[5] = "ඉ";vowelSinglish[5] = "i";vowelsSigns[5] = "ි";
	      vowelsUni[6] = "ඊ";vowelSinglish[6] = "I";vowelsSigns[6] = "ී";
	      vowelsUni[7] = "උ";vowelSinglish[7] = "u";vowelsSigns[7] = "ු";
	      vowelsUni[8] = "ඌ";vowelSinglish[8] = "U";vowelsSigns[8] = "ූ";
	      vowelsUni[9] = "ඓ";vowelSinglish[9] = "ei";vowelsSigns[9] = "ෛ";
	      vowelsUni[10] = "එ";vowelSinglish[10] = "e";vowelsSigns[10] = "ෙ";
	      vowelsUni[11] = "ඒ";vowelSinglish[11] = "E";vowelsSigns[11] = "ේ";
	      vowelsUni[12] = "ඔ";vowelSinglish[12] = "o";vowelsSigns[12] = "ො";
	      vowelsUni[13] = "ඕ";vowelSinglish[13] = "O";vowelsSigns[13] = "ෝ";
	      vowelsUni[14] = "සෘ";vowelSinglish[14] = "sru";vowelsSigns[14] = "ෘ";
	      vowelsUni[15] = "සෲ";vowelSinglish[15] = "srU";vowelsSigns[15] = "ෲ";
	      vowelsUni[16] = "ඏ";vowelSinglish[16] = "lr";vowelsSigns[16] = "ෟ";
	      vowelsUni[17] = "ඐ";vowelSinglish[17] = "Lr";vowelsSigns[17] = "ෳ";
	      
	      int consonants = 41;
	      String[] consonantsUni = new String[consonants];
	      String[] consonantsSinglish = new String[consonants];
	      
	      consonantsUni[0] = "ඟ";consonantsSinglish[0] = "gh";
	      consonantsUni[1] = "ඥ";consonantsSinglish[1] = "Gn";
	      consonantsUni[2] = "ඝ";consonantsSinglish[2] = "Gh";
	      consonantsUni[3] = "ච";consonantsSinglish[3] = "ch";
	      consonantsUni[4] = "ඡ";consonantsSinglish[4] = "Ch";
	      consonantsUni[5] = "ඣ";consonantsSinglish[5] = "jh";
	      consonantsUni[6] = "ඤ";consonantsSinglish[6] = "Kn";
	      consonantsUni[7] = "ඳ";consonantsSinglish[7] = "dH";
	      consonantsUni[8] = "ද";consonantsSinglish[8] = "dh";
	      consonantsUni[9] = "ඪ";consonantsSinglish[9] = "dD";
	      consonantsUni[10] = "ධ";consonantsSinglish[10] = "Dh";
	      consonantsUni[11] = "ඬ";consonantsSinglish[11] = "DH";
	      consonantsUni[12] = "ත";consonantsSinglish[12] = "th";
	      consonantsUni[13] = "ථ";consonantsSinglish[13] = "Th";
	      consonantsUni[14] = "ඵ";consonantsSinglish[14] = "ph";
	      consonantsUni[15] = "භ";consonantsSinglish[15] = "bh";
	      consonantsUni[16] = "ඞ";consonantsSinglish[16] = "nq";
	      consonantsUni[17] = "ශ";consonantsSinglish[17] = "sh";
	      consonantsUni[18] = "ෂ";consonantsSinglish[18] = "Sh";
	      consonantsUni[19] = "ක";consonantsSinglish[19] = "k";
	      consonantsUni[20] = "ඛ";consonantsSinglish[20] = "K";
	      consonantsUni[21] = "ග";consonantsSinglish[21] = "g";
	      consonantsUni[22] = "ජ";consonantsSinglish[22] = "j";
	      consonantsUni[23] = "ට";consonantsSinglish[23] = "t";
	      consonantsUni[24] = "ඨ";consonantsSinglish[24] = "T";
	      consonantsUni[25] = "ඩ";consonantsSinglish[25] = "d";
	      consonantsUni[26] = "ණ";consonantsSinglish[26] = "N";
	      consonantsUni[27] = "න";consonantsSinglish[27] = "n";
	      consonantsUni[28] = "ප";consonantsSinglish[28] = "p";
	      consonantsUni[29] = "බ";consonantsSinglish[29] = "b";
	      consonantsUni[30] = "ම";consonantsSinglish[30] = "m";
	      consonantsUni[31] = "ඹ";consonantsSinglish[31] = "B";
	      consonantsUni[32] = "ය";consonantsSinglish[32] = "y";
	      consonantsUni[33] = "ර";consonantsSinglish[33] = "r";
	      consonantsUni[34] = "ල";consonantsSinglish[34] = "l";
	      consonantsUni[35] = "ව";consonantsSinglish[35] = "v";
	      consonantsUni[36] = "ව";consonantsSinglish[36] = "w";
	      consonantsUni[37] = "ස";consonantsSinglish[37] = "s";
	      consonantsUni[38] = "හ";consonantsSinglish[38] = "h";
	      consonantsUni[39] = "ළ";consonantsSinglish[39] = "L";
	      consonantsUni[40] = "ෆ";consonantsSinglish[40] = "f";
	      
	      int specialChars = 3;
	      String[] specialCharUni = new String[specialChars];
	      String[] specialCharSinglish = new String[specialChars];
	      specialCharUni[0] = "්‍ර";specialCharSinglish[0] = "ra";
	      specialCharUni[1] = "ං";specialCharSinglish[1] = "q";
	      specialCharUni[2] = "ඃ";specialCharSinglish[2] = "Q";
	      

	      input = charReplace(input, vowelSinglish[3] + specialCharSinglish[1], vowelsUni[3] + specialCharUni[1]);
	      input = charReplace(input, vowelSinglish[3] + specialCharSinglish[2], vowelsUni[3] + specialCharUni[2]);
	      for (int i = 0; i < consonants; i++) {
	        for (int j = 0; j < vowels; j++)
	        {
	          String charNow = consonantsSinglish[i] + vowelSinglish[j];
	          String charnowUni = consonantsUni[i] + vowelsSigns[j];
	          input = charReplace(input, charNow, charnowUni);
	        }
	      }
	      for (int i = 0; i < consonants; i++) {
	        input = charReplace(input, consonantsSinglish[i], consonantsUni[i] + "්");
	      }
	      for (int i = 0; i < vowels; i++) {
	        input = charReplace(input, vowelSinglish[i], vowelsUni[i]);
	      }
	      for (int i = 0; i < specialChars; i++)
	      {
	        String charNow = specialCharSinglish[i];
	        String charnowUni = specialCharUni[i];
	        input = charReplace(input, charNow, charnowUni);
	      }
	    }
	    catch (ArrayIndexOutOfBoundsException e) {}
	    return input;
	  }
	  
	  public String charReplace(String _text, String _searchStr, String _replacementStr)
	  {
	    StringBuffer sb = new StringBuffer();
	    

	    int searchStringPos = _text.indexOf(_searchStr);
	    int startPos = 0;
	    int searchStringLength = _searchStr.length();
	    while (searchStringPos != -1)
	    {
	      sb.append(_text.substring(startPos, searchStringPos)).append(_replacementStr);
	      startPos = searchStringPos + searchStringLength;
	      searchStringPos = _text.indexOf(_searchStr, startPos);
	    }
	    sb.append(_text.substring(startPos, _text.length()));
	    
	    return sb.toString();
	  }
	  
	  public String UniToSin(String input)
	  {
	    try
	    {
	      int vowels = 18;
	      

	      String[] vowelsUni = new String[vowels];
	      String[] vowelSinglish = new String[vowels];
	      String[] vowelsSigns = new String[vowels];
	      

	      vowelsUni[0] = "ආ";vowelSinglish[0] = "aa";vowelsSigns[0] = "ා";
	      vowelsUni[1] = "ඇ";vowelSinglish[1] = "ae";vowelsSigns[1] = "ැ";
	      vowelsUni[2] = "ඖ";vowelSinglish[2] = "au";vowelsSigns[2] = "ෞ";
	      vowelsUni[3] = "ඈ";vowelSinglish[3] = "A";vowelsSigns[3] = "ෑ";
	      vowelsUni[4] = "ඉ";vowelSinglish[4] = "i";vowelsSigns[4] = "ි";
	      vowelsUni[5] = "ඊ";vowelSinglish[5] = "I";vowelsSigns[5] = "ී";
	      vowelsUni[6] = "උ";vowelSinglish[6] = "u";vowelsSigns[6] = "ු";
	      vowelsUni[7] = "ඌ";vowelSinglish[7] = "U";vowelsSigns[7] = "ූ";
	      vowelsUni[8] = "ඓ";vowelSinglish[8] = "ei";vowelsSigns[8] = "ෛ";
	      vowelsUni[9] = "එ";vowelSinglish[9] = "e";vowelsSigns[9] = "ෙ";
	      vowelsUni[10] = "ඒ";vowelSinglish[10] = "E";vowelsSigns[10] = "ේ";
	      vowelsUni[11] = "ඔ";vowelSinglish[11] = "o";vowelsSigns[11] = "ො";
	      vowelsUni[12] = "ඕ";vowelSinglish[12] = "O";vowelsSigns[12] = "ෝ";
	      vowelsUni[13] = "සෘ";vowelSinglish[13] = "sru";vowelsSigns[13] = "ෘ";
	      vowelsUni[14] = "සෲ";vowelSinglish[14] = "srU";vowelsSigns[14] = "ෲ";
	      vowelsUni[15] = "ඏ";vowelSinglish[15] = "lr";vowelsSigns[15] = "ෟ";
	      vowelsUni[16] = "ඐ";vowelSinglish[16] = "Lr";vowelsSigns[16] = "ෳ";
	      vowelsUni[17] = "අ";vowelSinglish[17] = "a";vowelsSigns[17] = "";
	      

	      int consonants = 41;
	      String[] consonantsUni = new String[consonants];
	      String[] consonantsSinglish = new String[consonants];
	      
	      consonantsUni[0] = "ඟ";consonantsSinglish[0] = "gh";
	      consonantsUni[1] = "ඥ";consonantsSinglish[1] = "Gn";
	      consonantsUni[2] = "ඝ";consonantsSinglish[2] = "Gh";
	      consonantsUni[3] = "ච";consonantsSinglish[3] = "ch";
	      consonantsUni[4] = "ඡ";consonantsSinglish[4] = "Ch";
	      consonantsUni[5] = "ඣ";consonantsSinglish[5] = "jh";
	      consonantsUni[6] = "ඤ";consonantsSinglish[6] = "Kn";
	      consonantsUni[7] = "ඳ";consonantsSinglish[7] = "dH";
	      consonantsUni[8] = "ද";consonantsSinglish[8] = "dh";
	      consonantsUni[9] = "ඪ";consonantsSinglish[9] = "dD";
	      consonantsUni[10] = "ධ";consonantsSinglish[10] = "Dh";
	      consonantsUni[11] = "ඬ";consonantsSinglish[11] = "DH";
	      consonantsUni[12] = "ත";consonantsSinglish[12] = "th";
	      consonantsUni[13] = "ථ";consonantsSinglish[13] = "Th";
	      consonantsUni[14] = "ඵ";consonantsSinglish[14] = "ph";
	      consonantsUni[15] = "භ";consonantsSinglish[15] = "bh";
	      consonantsUni[16] = "ඞ";consonantsSinglish[16] = "nq";
	      consonantsUni[17] = "ශ";consonantsSinglish[17] = "sh";
	      consonantsUni[18] = "ෂ";consonantsSinglish[18] = "Sh";
	      consonantsUni[19] = "ක";consonantsSinglish[19] = "k";
	      consonantsUni[20] = "ඛ";consonantsSinglish[20] = "K";
	      consonantsUni[21] = "ග";consonantsSinglish[21] = "g";
	      consonantsUni[22] = "ජ";consonantsSinglish[22] = "j";
	      consonantsUni[23] = "ට";consonantsSinglish[23] = "t";
	      consonantsUni[24] = "ඨ";consonantsSinglish[24] = "T";
	      consonantsUni[25] = "ඩ";consonantsSinglish[25] = "d";
	      consonantsUni[26] = "ණ";consonantsSinglish[26] = "N";
	      consonantsUni[27] = "න";consonantsSinglish[27] = "n";
	      consonantsUni[28] = "ප";consonantsSinglish[28] = "p";
	      consonantsUni[29] = "බ";consonantsSinglish[29] = "b";
	      consonantsUni[30] = "ම";consonantsSinglish[30] = "m";
	      consonantsUni[31] = "ඹ";consonantsSinglish[31] = "B";
	      consonantsUni[32] = "ය";consonantsSinglish[32] = "y";
	      consonantsUni[33] = "ර";consonantsSinglish[33] = "r";
	      consonantsUni[34] = "ල";consonantsSinglish[34] = "l";
	      consonantsUni[35] = "ව";consonantsSinglish[35] = "v";
	      consonantsUni[36] = "ව";consonantsSinglish[36] = "w";
	      consonantsUni[37] = "ස";consonantsSinglish[37] = "s";
	      consonantsUni[38] = "හ";consonantsSinglish[38] = "h";
	      consonantsUni[39] = "ළ";consonantsSinglish[39] = "L";
	      consonantsUni[40] = "ෆ";consonantsSinglish[40] = "f";
	      
	      int specialChars = 3;
	      String[] specialCharUni = new String[specialChars];
	      String[] specialCharSinglish = new String[specialChars];
	      specialCharUni[0] = "්‍ර";specialCharSinglish[0] = "ra";
	      specialCharUni[1] = "ං";specialCharSinglish[1] = "q";
	      specialCharUni[2] = "ඃ";specialCharSinglish[2] = "Q";
	      for (int i = 0; i < consonants; i++)
	      {
	        input = charReplace(input, consonantsUni[i] + "්", consonantsSinglish[i]);
	        for (int j = 0; j < vowels; j++) {
	          input = charReplace(input, consonantsUni[i] + vowelsSigns[j], consonantsSinglish[i] + vowelSinglish[j]);
	        }
	      }
	      for (int i = 0; i < vowels; i++) {
	        input = charReplace(input, vowelsUni[i], vowelSinglish[i]);
	      }
	    }
	    catch (ArrayIndexOutOfBoundsException e) {}
	    return input;
	  }
}
