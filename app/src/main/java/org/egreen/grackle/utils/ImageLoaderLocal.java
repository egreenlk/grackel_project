package org.egreen.grackle.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

/**
 * Created by dewmal on 8/20/14.
 */
public class ImageLoaderLocal {

    private static ImageLoaderConfiguration config;
    private static DisplayImageOptions options;
    private static ImageLoader imageLoader;

    public static final void load(Context context) {
        File cacheDir = StorageUtils.getCacheDirectory(context);
        config = new ImageLoaderConfiguration.Builder(context)
                .diskCache(new UnlimitedDiscCache(cacheDir))
                .build();


        options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }

    public static ImageLoader getImageLoader() {
        imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            imageLoader.init(config);
        }
        return imageLoader;
    }


    public static ImageLoaderConfiguration getConfig() {
        return config;
    }

    public static DisplayImageOptions getOptions() {
        return options;
    }
}
