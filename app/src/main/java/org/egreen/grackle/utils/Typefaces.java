package org.egreen.grackle.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

public final class Typefaces {
    private static final String TAG = "Typefaces";

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String name) {
        synchronized (cache) {
            if (!cache.containsKey(name)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(),
                            String.format("fonts/%s.ttf", name));
                    cache.put(name, t);
                    return t;
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Could not get typeface '" + name
                            + "' because " + e);
                    return null;
                }
            } else {
                return cache.get(name);
            }
        }
    }
}
