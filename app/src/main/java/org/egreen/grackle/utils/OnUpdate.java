package org.egreen.grackle.utils;

/**
 * Created by dewmal on 8/18/14.
 */
public interface OnUpdate<Result> {
    void updateResult(Result result);
}
