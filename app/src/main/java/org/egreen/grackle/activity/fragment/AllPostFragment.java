package org.egreen.grackle.activity.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nirhart.parallaxscroll.views.ParallaxListView;

import org.egreen.grackle.R;
import org.egreen.grackle.model.controller.CategoryObjectEntityController;
import org.egreen.grackle.server.category.model.CategoryObject;
import org.egreen.grackle.server.posting.model.PostObject;
import org.egreen.grackle.server.tasks.PostListRequestTask;
import org.egreen.grackle.ui.CardUiItemView;
import org.egreen.grackle.ui.bluractionbar.Blur;
import org.egreen.grackle.ui.util.BlurImage;
import org.egreen.grackle.utils.OnUpdate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dewmal on 8/3/14.
 */
public class AllPostFragment extends Fragment implements OnUpdate<List<PostObject>> {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = AllPostFragment.class.getName();

    private Context context;

    //Array for post
    private List<PostObject> postList;
    // Post View List Adapter
    private CardUiItemView newsArrayAdapter;

    // On Post Update Listner
    private ParallaxListView listView;

    //
    private Handler handler = new Handler();
    private String categoryTitle;
    private CategoryObject category;
    private ProgressDialog progressialog;


    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        if (context == null) {
            context = getActivity();
        }
        return context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ParallaxListView) rootView.findViewById(R.id.news_list_view);
        //Log.i(TAG, listView + "70");
        int category_position = getArguments().getInt("category_position");


        category = CategoryObjectEntityController.getCategory(category_position);
        if (category != null) {
            categoryTitle = category.getCategory();
        }


        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(categoryTitle);
        actionBar.setHomeButtonEnabled(true);


        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressialog = ProgressDialog.show(getContext(), "",
                "Loading. Please wait...", true);

        //Request Get All post Objects
        PostListRequestTask postListRequestTask = new PostListRequestTask(this, this.getActivity(), category);
        postListRequestTask.execute();
        progressialog.show();
    }

    @Override
    public void updateResult(final List<PostObject> postObjects) {
        postList = postObjects;


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (postList != null) {
                    Log.i(TAG, Arrays.toString(postList.toArray()) + "");
                    Log.i(TAG, postObjects.size() + " " + getContext() + " ");
                    newsArrayAdapter = new CardUiItemView(getContext(), postList);
                    listView.setAdapter(newsArrayAdapter);
                } else {
                    Toast.makeText(getContext(), "Sorry No Content Available for this category", Toast.LENGTH_LONG).show();
                }
                progressialog.dismiss();

            }
        }, 1);

    }


}


