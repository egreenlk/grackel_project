package org.egreen.grackle.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.manuelpeinado.fadingactionbar.extras.actionbarcompat.FadingActionBarHelper;
import com.manuelpeinado.fadingactionbar.view.RootLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.egreen.grackle.ApplicationGrackel;
import org.egreen.grackle.R;
import org.egreen.grackle.server.posting.model.PostObject;
import org.egreen.grackle.server.posting.model.Resources;
import org.egreen.grackle.utils.ImageLoaderLocal;
import org.egreen.grackle.utils.Typefaces;

import java.util.ArrayList;
import java.util.List;

public class SelectPostViewActivity extends ActionBarActivity {


    private TextView tvPostViewDetails;

    private static PostObject postObject;
    private TextView tvPostTitle;
    private ImageView imgView;
    private static final String TAG = SelectPostViewActivity.class.getName();


    private ImageButton btnImdbLink;
    private ImageButton btnYoutubeLink;
    private ImageButton btnSubDwnBtn;
    private ImageButton btnMovieDwnBtn;
    private String movieTrailer;
    private String imdbLink;
    private String sub_title_1;
    private String sub_title_2;
    private String sub_title_3;
    private String sub_title_4;


    private AdView adView;
    private List<String> dwn_List_Name;
    private List<String> dwn_List_Link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageLoader imageLoader = ImageLoaderLocal.getImageLoader();
        dwn_List_Name = new ArrayList<String>();
        dwn_List_Link = new ArrayList<String>();

        //getActionBar().getCustomView().setAnimation(Animation.);
// ScrollView scrollView=findViewById(R.id.selected_post_view_details_view);


        if (postObject != null) {
            String imageUrl = "http://www.zoom.lk/TV/Teenwolf4/teenepi9.jpg";
            for (Resources res : postObject.getResourceses()) {
                if ("movie_trailer".contentEquals(res.getName())) {
                    movieTrailer = res.getResURL();
                } else if ("imdb_link".contentEquals(res.getName())) {
                    imdbLink = res.getResURL();
                } else if ("banner_image".contentEquals(res.getName())) {
                    imageUrl = res.getResURL();
                } else if ("sinhala_sub1".contentEquals(res.getName())) {
                    sub_title_1 = res.getResURL();
                } else if ("sinhala_sub2".contentEquals(res.getName())) {
                    sub_title_2 = res.getResURL();
                } else if ("sinhala_sub3".contentEquals(res.getName())) {
                    sub_title_3 = res.getResURL();
                } else if ("sinhala_sub4".contentEquals(res.getName())) {
                    sub_title_4 = res.getResURL();
                } else {
                    String link = null;
                    String name = null;
                    if ("720p_torrent_link".contentEquals(res.getName())) {
                        name = "720P Torrent";
                    } else if ("1080p_torrent_link".contentEquals(res.getName())) {
                        name = "1080p Torrent";
                    } else if ("720p_direct_link".contentEquals(res.getName())) {
                        name = "720P Direct";
                    } else if ("1080p_direct_link".contentEquals(res.getName())) {
                        name = "720P Direct";
                    } else {

                        name = res.getName();
                    }
                    link = res.getResURL();
                    if (link != null && name != null && !link.isEmpty() && !name.isEmpty()) {
                        dwn_List_Name.add(name);
                        dwn_List_Link.add(link);
                    }
                }
            }

            LayoutInflater inflater = this.getLayoutInflater();
            ImageView headerView = (ImageView) inflater.inflate(R.layout.header, null, false);
            Bitmap loadImageSync = imageLoader.loadImageSync(imageUrl);
            headerView.setImageBitmap(loadImageSync);

            FadingActionBarHelper helper = new FadingActionBarHelper()
                    .headerView(headerView)
                    .actionBarBackground(new ColorDrawable(getResources().getColor(R.color.navdrawer_text_color_selected)))
                    .contentLayout(R.layout.activity_select_post_view);
            RootLayout helperView = (RootLayout) helper.createView(this);


            RelativeLayout mainView = (RelativeLayout) inflater.inflate(R.layout.selected_item_view, null, false);
            RootLayout rootLayout = (RootLayout) mainView.findViewById(R.id.view_root_layer);
            View rootView = helperView.getRootView();
            rootLayout.addView(rootView);


//            LinearLayout layout=new LinearLayout(this);
//            layout.setOrientation(LinearLayout.VERTICAL);
//            layout.addView(helperView);
//            layout.addView(adView);
            //helperView.addView(adView,layoutParams);
            //  Log.i(TAG,helperView.get+"");


            setContentView(mainView);


            adView = (AdView) mainView.findViewById(R.id.adView);

            tvPostViewDetails = (TextView) findViewById(R.id.selected_post_view_details);
            btnImdbLink = (ImageButton) findViewById(R.id.selected_post_btn_imdb);
            btnSubDwnBtn = (ImageButton) findViewById(R.id.selected_post_btn_dwn_sub);
            btnYoutubeLink = (ImageButton) findViewById(R.id.selected_post_btn_youtube);
            btnMovieDwnBtn = (ImageButton) findViewById(R.id.selected_post_btn_dwn_torrent);

            //   tvPostTitle = (TextView) findViewById(R.id.selected_post_view_details_title);
            //   imgView = (ImageView) findViewById(R.id.selected_post_view_details_image);

            tvPostViewDetails.setTypeface(Typefaces.get(this, "LBhashitaComplex"));
            tvPostViewDetails.setText((postObject.getContent()));


//          vPostViewDetails.setText(LanConverterEngine.convertToView(postObject.getContent()));


            btnImdbLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(imdbLink));
                    startActivity(browserIntent);
                }
            });

            btnYoutubeLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(movieTrailer));
                    startActivity(browserIntent);
                }
            });

            btnSubDwnBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCreateDialog().show();
                }
            });

            btnMovieDwnBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCreateDwnMovieDialog().show();
                }
            });


            if (dwn_List_Link == null || dwn_List_Link.isEmpty()) {
                btnMovieDwnBtn.setEnabled(false);
            }

            helper.parallax(true).lightActionBar(true).initActionBar(this);


            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(postObject.getTitle());
            actionBar.setHomeButtonEnabled(true);

        }


        // Get tracker.
        Tracker t = ((ApplicationGrackel) getApplication()).getTracker(
                ApplicationGrackel.TrackerName.APP_TRACKER);

        // Set screen name.
        // Where path is a String representing the screen name.
        t.setScreenName(TAG);

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());


        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);

    }


    public static void setPostObject(PostObject postObject) {
        SelectPostViewActivity.postObject = postObject;
    }


    public Dialog onCreateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dwn_load_sub)
                .setItems(R.array.download_subs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Log.i(TAG, which + "");

                        switch (which) {
                            case 0: {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sub_title_1));
                                startActivity(browserIntent);
                            }
                            break;
                            case 1: {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sub_title_2));
                                startActivity(browserIntent);
                            }
                            break;
                            case 2: {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sub_title_3));
                                startActivity(browserIntent);
                            }
                            break;
                            case 3: {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sub_title_4));
                                startActivity(browserIntent);
                            }
                            break;
                        }
                    }
                }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }


    public Dialog onCreateDwnMovieDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dwn_load_sub)
                .setItems(dwn_List_Name.toArray(new String[]{}), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Log.i(TAG, which + "");

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dwn_List_Link.get(which)));
                        startActivity(browserIntent);

                    }
                }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    /**
     * Called before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        // Destroy the AdView.
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}
