package org.egreen.grackle.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.egreen.grackle.R;
import org.egreen.grackle.activity.SelectPostViewActivity;
import org.egreen.grackle.server.posting.model.PostObject;
import org.egreen.grackle.server.posting.model.Resources;
import org.egreen.grackle.utils.ImageLoaderLocal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dewmal on 8/3/14.
 */
public class CardUiItemView extends ArrayAdapter<PostObject> {

    private static final String TAG = CardUiItemView.class.getName();
    private final ImageLoader imageLoader;
    private Context context;
    private List<PostObject> newsList = new ArrayList<PostObject>();


    public CardUiItemView(Context context, List<PostObject> newsList) {
        super(context, R.layout.list_item_card, newsList);
        this.context = context;
        this.newsList = newsList;

        imageLoader = ImageLoaderLocal.getImageLoader();
    }


    Handler handler = new Handler();

    @Override
    public View getView(int position, View rowView, ViewGroup parent) {
        final PostObject news = newsList.get(position);
        Log.i(TAG, news + "");
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (rowView == null) {
            rowView = layoutInflater.inflate(R.layout.list_item_card, null);
            holder = new ViewHolder();
            holder.titleView = (TextView) rowView.findViewById(R.id.post_list_item_title);
            holder.imageView = (ImageView) rowView.findViewById(R.id.news_list_image);
            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }


        holder.titleView.setText(news.getTitle());
        String imageUrl = "http://www.zoom.lk/TV/Teenwolf4/teenepi9.jpg";
        for (Resources res : news.getResourceses()) {
            Log.i(TAG, res + "");
            imageUrl = res.getResURL();
            break;
        }
        imageLoader.displayImage(imageUrl, holder.imageView, ImageLoaderLocal.getOptions());

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SelectPostViewActivity.class);
                SelectPostViewActivity.setPostObject(news);
                context.startActivity(intent);
            }
        });


        //ImageView mainImage = (ImageView) rowView.findViewById(R.id.news_list_image);
        //mainImage.setImageDrawable(news.getResourceses().);


        return rowView;
    }

    static class ViewHolder {
        TextView titleView;
        ImageView imageView;
    }
}
