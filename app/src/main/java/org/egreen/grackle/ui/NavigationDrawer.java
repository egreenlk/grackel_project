package org.egreen.grackle.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.egreen.grackle.R;
import org.egreen.grackle.server.category.model.CategoryObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dewmal on 8/2/14.
 */
public class NavigationDrawer extends ArrayAdapter<CategoryObject> {


    Context context;
    private List<CategoryObject> TextValue = new ArrayList<CategoryObject>();
    private int selectedItem;

    public NavigationDrawer(Context context,List<CategoryObject> TextValue) {
        super(context, R.layout.row_of_drawer, TextValue);
        this.context = context;
        this.TextValue = TextValue;
    }


    @Override
    public View getView(int position, View coverView, ViewGroup parent) {
        // TODO Auto-generated method stub

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_of_drawer,
                parent, false);

        final TextView text1 = (TextView) rowView.findViewById(R.id.textView);
        text1.setText(TextValue.get(position).getCategory());

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_name));




        //Change Color on selected Item
        if(position==selectedItem){
         //   rowView.setBackgroundColor(context.getResources().getColor(R.color.navdrawer_icon_tint_selected));
            text1.setTextColor(context.getResources().getColor(R.color.theme_accent_2));

        }

        return rowView;

    }


    /**
     * Fire on item select
     * @param selectedItem
     */
    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }


}
