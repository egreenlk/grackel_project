package org.egreen.grackle.ui.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.RelativeLayout;

import org.egreen.grackle.R;
import org.sufficientlysecure.htmltextview.HtmlTextView;

/**
 * Created by dewmal on 9/2/14.
 */
public class MessageDialogBuilder {

    /**
     * About us dialog
     */
    public static void showAboutDialog(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle("About us");

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, close
                                // current activity
                                dialog.dismiss();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        RelativeLayout aboutLayout = (RelativeLayout) alertDialog.getLayoutInflater().inflate(R.layout.about_layout, null);
        HtmlTextView htmlTextView = (HtmlTextView) aboutLayout.findViewById(R.id.about_text_view);
        htmlTextView.setHtmlFromString(" <p style='text-align:justify;'> <h4>Zoom.lk - Beyond the Average</h4>\n" +
                "\n" +
                "                                <p>Product Version: 1</p>\n" +
                "\n" +
                "                                <p>Build: 1</p>\n" +
                "                                <p>Runtime: Android 2.3</p>\n" +
                "\n" +
                "                                <p>Home Page: <a href='http://www.zoom.lk'>www.zoom.lk</a></p>\n" +
                "\n" +
                "                                <p>Sri Lanka's premier Sinhala subtitles portal</p> </p>", false);

        alertDialog.setView(aboutLayout);
        // show it
        alertDialog.show();

    }
}
