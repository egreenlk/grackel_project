package org.egreen.grackle.model;

import android.nfc.Tag;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import org.egreen.grackle.server.category.model.CategoryObject;

/**
 * Created by dewmal on 8/18/14.
 */
@Table(name = "category")
public class CategoryObjectEntity extends Model {

    private static final String TAG = CategoryObjectEntity.class.getName();
    @Column(name = "category")
    private String category;

    @Column(name = "cat_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private Long cat_id;

    @Column(name = "parentCategory")
    private Long parentCategory;

    public CategoryObjectEntity() {
    }

    public CategoryObjectEntity(CategoryObject categoryObject) {
        if (categoryObject != null) {
            category = categoryObject.getCategory();
            cat_id = categoryObject.getId();
            parentCategory = categoryObject.getParentCategory();
        }
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public Long getCat_id() {
        return cat_id;
    }

    public void setCat_id(Long cat_id) {
        this.cat_id = cat_id;
    }

    public Long getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Long parentCategory) {
        this.parentCategory = parentCategory;
    }


    @Override
    public String toString() {
        return "CategoryObjectEntity{" +
                "category='" + category + '\'' +
                ", cat_id=" + cat_id +
                ", parentCategory=" + parentCategory +
                '}';
    }

    /**
     *
     * @param objectEntity
     * @return
     */
    public static CategoryObject getCategoryObject(CategoryObjectEntity objectEntity) {
        CategoryObject categoryObject = new CategoryObject();

        if (objectEntity != null) {
            categoryObject.setCategory(objectEntity.getCategory());
            categoryObject.setId(objectEntity.getCat_id());
            categoryObject.setParentCategory(objectEntity.getParentCategory());
        }
        Log.i(TAG,categoryObject+"");
        return categoryObject;
    }
}
