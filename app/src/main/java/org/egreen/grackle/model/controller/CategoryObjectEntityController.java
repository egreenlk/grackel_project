package org.egreen.grackle.model.controller;

import android.util.Log;

import com.activeandroid.query.Select;

import org.egreen.grackle.model.CategoryObjectEntity;
import org.egreen.grackle.server.category.model.CategoryObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dewmal on 8/19/14.
 */
public class CategoryObjectEntityController {


    private static final String TAG = CategoryObjectEntityController.class.getName();

    public static final List<CategoryObject> getCategoryObjectList() {
        List<CategoryObject> cateObjects = new ArrayList<CategoryObject>();
        List<CategoryObjectEntity> execute = new Select().from(CategoryObjectEntity.class).orderBy("cat_id").execute();
        for (CategoryObjectEntity objectEntity : execute) {
            if (objectEntity.getParentCategory() == null) {
                Log.i(TAG, objectEntity + "");

                cateObjects.add(CategoryObjectEntity.getCategoryObject(objectEntity));
            }
        }
        cateObjects.addAll(getSubCategoryObjectList());
        return cateObjects;
    }

    public static final List<CategoryObject> getSubCategoryObjectList() {
        List<CategoryObject> cateObjects = new ArrayList<CategoryObject>();
        List<CategoryObjectEntity> execute = new Select().from(CategoryObjectEntity.class).orderBy("cat_id").execute();
        for (CategoryObjectEntity objectEntity : execute) {
            if (objectEntity.getParentCategory() != null) {
                cateObjects.add(CategoryObjectEntity.getCategoryObject(objectEntity));
            }
        }
        return cateObjects;
    }


    public static CategoryObject getCategory(int category_position) {
        CategoryObject categoryObject = getCategoryObjectList().get(category_position);
        return categoryObject;
    }
}
