package org.egreen.grackle.server;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

import org.egreen.grackle.server.category.Category;
import org.egreen.grackle.server.posting.Posting;
import org.egreen.grackle.server.registration.Registration;

/**
 * Created by dewmal on 8/17/14.
 */
public class AppConstants {


    /**
     * Your WEB CLIENT ID from the API Access screen of the Developer Console for your project. This
     * is NOT the Android client id from that screen.
     *
     * @see <a href="https://developers.google.com/console">https://developers.google.com/console</a>
     */
    public static final String WEB_CLIENT_ID = "691431473781-f3lp2bfma4t8hv0vo1t7dqvtvbj9to2d.apps.googleusercontent.com";

    /**
     * The audience is defined by the web client id, not the Android client id.
     */
    public static final String AUDIENCE = "server:client_id:" + WEB_CLIENT_ID;

    /**
     * Class instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new AndroidJsonFactory();

    /**
     * Class instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();

    /**
     * Count Google accounts on the device.
     */
    public static int countGoogleAccounts(Context context) {
        AccountManager am = AccountManager.get(context);
        Account[] accounts = am.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (accounts == null || accounts.length < 1) {
            return 0;
        } else {
            return accounts.length;
        }
    }

    /**
     * Retrieve a Helloworld api service handle to access the API.
     */
    public static Posting getApiServiceHandle(@Nullable GoogleAccountCredential credential) {
        // Use a builder to help formulate the API request.
        Posting.Builder posting = new Posting.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY, credential);

        // If running the Cloud Endpoint API locally then point the API stub there by un-commenting the
        // next line.
        // helloWorld.setRootUrl("http://192.168.1.100:8080/_ah/api/");

        return posting.build();
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     */
    public static boolean checkGooglePlayServicesAvailable(Activity activity) {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(activity, connectionStatusCode);
            return false;
        }
        return true;
    }

    /**
     * Called if the device does not have Google Play Services installed.
     */
    public static void showGooglePlayServicesAvailabilityErrorDialog(final Activity activity,
                                                                     final int connectionStatusCode) {
        final int REQUEST_GOOGLE_PLAY_SERVICES = 0;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, activity, REQUEST_GOOGLE_PLAY_SERVICES);
                dialog.show();
            }
        });
    }

    public static Registration getResgistrationHandler(@Nullable GoogleAccountCredential credential) {
        // Use a builder to help formulate the API request.
        Registration.Builder builder = new Registration.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY, credential);

        // If running the Cloud Endpoint API locally then point the API stub there by un-commenting the
        // next line.
        // helloWorld.setRootUrl("http://192.168.1.100:8080/_ah/api/");

        return builder.build();
    }

    public static Category getCategoryApiInstance(@Nullable Credential credential) {
        // Use a builder to help formulate the API request.
        Category.Builder builder = new Category.Builder(AppConstants.HTTP_TRANSPORT,
                AppConstants.JSON_FACTORY, credential);

        // If running the Cloud Endpoint API locally then point the API stub there by un-commenting the
        // next line.
        // helloWorld.setRootUrl("http://192.168.1.100:8080/_ah/api/");

        return builder.build();
    }
}
