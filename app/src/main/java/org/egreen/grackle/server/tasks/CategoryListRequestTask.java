package org.egreen.grackle.server.tasks;

import android.os.AsyncTask;

import org.egreen.grackle.server.AppConstants;
import org.egreen.grackle.server.category.Category;
import org.egreen.grackle.server.category.model.CategoryObject;
import org.egreen.grackle.utils.OnUpdate;

import java.io.IOException;
import java.util.List;

/**
 * Created by dewmal on 8/18/14.
 */
public class CategoryListRequestTask extends AsyncTask<Void, Integer, List<CategoryObject>> {

    //Update on post execute
    private final OnUpdate<List<CategoryObject>> onUpdate;


    public CategoryListRequestTask(OnUpdate<List<CategoryObject>> onUpdate) {
        this.onUpdate = onUpdate;
    }

    @Override
    protected List<CategoryObject> doInBackground(Void... voids) {
        Category category = AppConstants.getCategoryApiInstance(null);
        try {
            return category.list().execute().getItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(List<CategoryObject> categoryObjects) {
        onUpdate.updateResult(categoryObjects);
    }
}
