package org.egreen.grackle.server.tasks;

import android.app.Activity;
import android.os.AsyncTask;

import org.egreen.grackle.server.AppConstants;
import org.egreen.grackle.server.category.model.CategoryObject;
import org.egreen.grackle.server.posting.Posting;
import org.egreen.grackle.server.posting.model.PostObject;
import org.egreen.grackle.server.posting.model.PostObjectCollection;
import org.egreen.grackle.utils.OnUpdate;

import java.io.IOException;
import java.util.List;

/**
 * Created by dewmal on 8/17/14.
 */
public class PostListRequestTask extends AsyncTask<Void, Long, List<PostObject>> {


    private final OnUpdate<List<PostObject>> listOnUpdate;


    private static final String TAG = PostListRequestTask.class.getName();
    private final Activity activity;
    private final CategoryObject category;
    private long categoryId = 0;



    public PostListRequestTask(OnUpdate<List<PostObject>> listOnUpdate, Activity activity, CategoryObject category) {
        this.listOnUpdate = listOnUpdate;
        this.activity = activity;
        this.category = category;
    }

    @Override
    protected List<PostObject> doInBackground(Void... voids) {
        Posting posting = AppConstants.getApiServiceHandle(null);
        PostObjectCollection postObjectCollection = null;


        try {
            if (category != null && category.getId() != 0) {
                Posting.Getallbycategory getall = posting.getallbycategory(100, 0, category.getId());
                postObjectCollection = getall.execute();
            } else {
                Posting.Getall getall = posting.getall(100, 0);
                postObjectCollection = getall.execute();
            }
            List<PostObject> items = postObjectCollection.getItems();
//            Toast.makeText(activity, items + "", Toast.LENGTH_LONG).show();
            return items;
//            Toast.makeText(activity, postObject + "", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<PostObject> postObjects) {
        listOnUpdate.updateResult(postObjects);
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        super.onProgressUpdate(values);
    }
}
